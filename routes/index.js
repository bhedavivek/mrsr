module.exports = {
    authenticate : require('./authenticate'),
    api : require('./api'),
    admin : require('./admin'),
    register : require('./register'),
    default : require('./default')
}