var express = require('express');
var bodyParser = require('body-parser');
var https = require('https');
var app = express();
var router = express.Router();
var routes = require('./routes/index');
var config = require('./config/config')

//HTTPS OPTIONS
var options = {
    cert : config.certificate,
    key : config.private
}

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

//ROUTE TO DEFAULT ROUTER
app.post('/*',function(req, res){
    console.log(req.body);
    res.json(req.body);
});

app.disable('x-powered-by');

//CREATING HTTPS SERVER
app.listen(3000);

console.log("Node Server(Development) is running on PORT : 8001");